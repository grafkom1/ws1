# Implementasi Paint dengan WebGL

## Kontributor

- Faza Aulia Prayoga - 1806173525
- Muhammad Tsaqif Al Bari - 1806235731
- Naufal Alauddin Hilmi - 1806205754

## Cara penggunaan

Untuk memulai dapat membuka file cad2.html pada browser

Pada aplikasi terdapat 3 dropdown:

1. Warna
2. Bentuk
3. Tebal garis (hanya dapat terlihat setelah memilih bentuk _line_)

Pertama, atur warna dan bentuk apa yang ingin digambar dengan menentukan opsi pada dropdown.
Khusus untuk opsi bentuk _Line_, terdapat opsi tambahan yaitu ketebalan garis.

Kemudian terdapat checkbox _Fill_ untuk menentukan apakah gambar yang ingin
diisi warna atau berupa garis saja. Opsi tersebut tidak berpengaruh pada opsi bentuk _Line_.

Setelah mengatur opsi, silahkan klik pada bagian canvas untuk menggambar bentuk
tersebut, khusus untuk bentuk _polygon_, tedapat button _end polygon_ untuk
menyelesaikan bentuk.

Setelah menggambar bentuk, tedapat button _Animate_ yang akan menggerakan semua
bentuk pada canvas.

Kemudian terdapat button _clear screen_ untuk menghapus semua gambar pada canvas

## External Link

Aplikasi juga dapat diakses pada link berikut: [https://grafkom1.gitlab.io/ws1/cad2.html](https://grafkom1.gitlab.io/ws1/cad2.html)
