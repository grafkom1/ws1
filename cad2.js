"use strict";

var canvas;
var gl;

var maxNumPositions = 200;

// index on buffer
var index = 0;
var cindex = 0;

// color definition
var colors = [
  vec4(0.0, 0.0, 0.0, 1.0), // black
  vec4(1.0, 0.0, 0.0, 1.0), // red
  vec4(1.0, 1.0, 0.0, 1.0), // yellow
  vec4(0.0, 1.0, 0.0, 1.0), // green
  vec4(0.0, 0.0, 1.0, 1.0), // blue
  vec4(1.0, 0.0, 1.0, 1.0), // magenta
  vec4(0.0, 1.0, 1.0, 1.0), // cyan
];

// TODO: find a way to generalise tSquare
//       to other shapes
// rectangle
var tSquare = [];

// current type of shape
var shapeType;
var shapeFill = false;

// accumulated points for polygon
var accumulatedPoints = 0;

// accumulated objects
var shapes = [];

// set up sudut rotasi awal di program
var theta = 0.0;

// sudut rotasi di memori
var thetaLoc;

// waktu timeout antar render jika animate toggle on
var speed = 100;

// animation toggle
var animate = false;

// line thickness
var lineThickness = "thin";

class Shape {
  constructor(startingIndex, numberOfPoints, isFilled) {
    this.startingIndex = startingIndex;
    this.numberOfPoints = numberOfPoints;
    this.isFilled = isFilled;
  }
}

class Rectangle extends Shape {
  constructor(startingIndex, isFilled) {
    super(startingIndex, 4, isFilled);
  }
}

class Triangle extends Shape {
  constructor(startingIndex, isFilled) {
    super(startingIndex, 3, isFilled);
  }
}

function canvasEventHandler(gl, canvas, bufferId, cBufferId) {
  return function (event) {
    var canvasOffset = canvas.getBoundingClientRect();
    var mousePosition = vec2(
      (2 * (event.clientX - canvasOffset.left)) / canvas.width - 1,
      (2 * (canvas.height - (event.clientY - canvasOffset.top))) /
        canvas.height -
        1
    );

    switch (shapeType) {
      case "polygon":
        // write point to buffer
        gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
        gl.bufferSubData(gl.ARRAY_BUFFER, 8 * index, flatten(mousePosition));

        var currentColor = vec4(colors[cindex]);

        gl.bindBuffer(gl.ARRAY_BUFFER, cBufferId);
        gl.bufferSubData(gl.ARRAY_BUFFER, 16 * index, flatten(currentColor));

        accumulatedPoints++;
        index++;
        break;

      case "rectangle":
        if (accumulatedPoints < 1) {
          accumulatedPoints = 1;
          tSquare[0] = mousePosition;
        } else {
          // bind buffer to write vertex
          gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);

          // set up rectangle vertices
          accumulatedPoints = 0;
          tSquare[2] = mousePosition;
          tSquare[1] = vec2(tSquare[0][0], tSquare[2][1]);
          tSquare[3] = vec2(tSquare[2][0], tSquare[0][1]);

          // write rectangle vertices to buffer
          for (var i = 0; i < 4; i++)
            gl.bufferSubData(
              gl.ARRAY_BUFFER,
              8 * (index + i),
              flatten(tSquare[i])
            );

          // bind color buffer
          gl.bindBuffer(gl.ARRAY_BUFFER, cBufferId);

          // write color to buffer
          var currentColor = vec4(colors[cindex]);
          for (var i = 0; i < 4; i++)
            gl.bufferSubData(
              gl.ARRAY_BUFFER,
              16 * (index + i),
              flatten(currentColor)
            );

          /// push shape to shapes list
          shapes.push(new Rectangle(index, shapeFill));
          index += 4;
        }
        break;

      case "triangle":
        // write to vertex buffer
        gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
        gl.bufferSubData(gl.ARRAY_BUFFER, 8 * index, flatten(mousePosition));

        var currentColor = vec4(colors[cindex]);

        // write to color buffer
        gl.bindBuffer(gl.ARRAY_BUFFER, cBufferId);
        gl.bufferSubData(gl.ARRAY_BUFFER, 16 * index, flatten(currentColor));

        // increment point
        accumulatedPoints++;
        index++;

        // push triangle to shape list after 3 points
        if (accumulatedPoints === 3) {
          accumulatedPoints = 0;
          shapes.push(new Triangle(index - 3, shapeFill));
        }
        break;
      case "line":
        // pilih opsi ketebalan garis
        var thickness;
        switch (lineThickness) {
          case "thin":
            thickness = 0.001;
            break;
          case "normal":
            thickness = 0.005;
            break;
          case "thick":
            thickness = 0.01;
            break;
        }
        if (accumulatedPoints < 1) {
          accumulatedPoints = 1;
          tSquare[0] = mousePosition;
        } else {
          // Cari arah garis, gradien
          var a = tSquare[0];
          var b = mousePosition;
          var m = (b[1] - a[1]) / (b[0] - a[0]);

          // Cari garis tegak lurus dengan gradien
          var m_perpen = -1 / m;

          var xDifferences = thickness / Math.sqrt(1 + m_perpen ** 2);
          var yDifferences =
            (thickness * m_perpen) / Math.sqrt(1 + m_perpen ** 2);

          // buat rectangle dengan kedua gradien
          tSquare[1] = vec2(a[0] + xDifferences, a[1] + 2 * yDifferences);
          tSquare[2] = vec2(b[0] + xDifferences, b[1] + 2 * yDifferences);
          tSquare[3] = mousePosition;

          gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
          for (var i = 0; i < 4; i++)
            gl.bufferSubData(
              gl.ARRAY_BUFFER,
              8 * (index + i),
              flatten(tSquare[i])
            );

          // bind color buffer
          gl.bindBuffer(gl.ARRAY_BUFFER, cBufferId);

          // write color to buffer
          var currentColor = vec4(colors[cindex]);
          for (var i = 0; i < 4; i++)
            gl.bufferSubData(
              gl.ARRAY_BUFFER,
              16 * (index + i),
              flatten(currentColor)
            );

          shapes.push(new Rectangle(index, true));
          accumulatedPoints = 0;

          index += 4;
        }
        break;
    }
  };
}

function endPolygonButtonHandler() {
  if (shapeType !== "polygon") return;

  shapes.push(
    new Shape(index - accumulatedPoints, accumulatedPoints, shapeFill)
  );

  //reset accumulated points for polygon
  accumulatedPoints = 0;
}

init();

function init() {
  canvas = document.getElementById("gl-canvas");

  gl = canvas.getContext("webgl2");
  if (!gl) alert("WebGL 2.0 isn't available");

  gl.viewport(0, 0, canvas.width, canvas.height);
  gl.clearColor(0.8, 0.8, 0.8, 1.0);
  gl.clear(gl.COLOR_BUFFER_BIT);

  //
  //  Load shaders and initialize attribute buffers
  //
  var program = initShaders(gl, "vertex-shader", "fragment-shader");
  gl.useProgram(program);

  var bufferId = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
  gl.bufferData(gl.ARRAY_BUFFER, 8 * maxNumPositions, gl.STATIC_DRAW);
  var postionLoc = gl.getAttribLocation(program, "aPosition");
  gl.vertexAttribPointer(postionLoc, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(postionLoc);

  var cBufferId = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, cBufferId);
  gl.bufferData(gl.ARRAY_BUFFER, 16 * maxNumPositions, gl.STATIC_DRAW);
  var colorLoc = gl.getAttribLocation(program, "aColor");
  gl.vertexAttribPointer(colorLoc, 4, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(colorLoc);

  thetaLoc = gl.getUniformLocation(program, "uTheta");

  // Setup event Handler

  document.getElementById("color").addEventListener("change", function (event) {
    cindex = event.target.value;
  });

  // set up event handler to end polygon line
  var endPolygonButton = document.getElementById("end-polygon");

  endPolygonButton.addEventListener("click", endPolygonButtonHandler);

  // setup listener untuk ketebalan garis
  var lineThicknessOption = document.getElementById("line-thickness")
  lineThicknessOption
    .addEventListener("change", function (event) {
      lineThickness = event.target.value;
    });

  var shapeOption = document.getElementById("type");

  // initialize shapeType
  shapeType = shapeOption.value;

  // change shape type on value change
  shapeOption.addEventListener("change", function (event) {
    shapeType = event.target.value;
    accumulatedPoints = 0;
    if (event.target.value !== "polygon") {
      endPolygonButton.style.display = "none";
    } else {
      endPolygonButton.style.display = null;
    }

    if (event.target.value === "line") {
      lineThicknessOption.style.display = null;
    } else {

      lineThicknessOption.style.display = "none";
    }
  });

  var fillOption = document.getElementById("filled");
  fillOption.addEventListener("click", function (event) {
    shapeFill = fillOption.checked;
  });

  // setup listener untuk melihat jika user menekan canvas
  canvas.addEventListener(
    "mousedown",
    canvasEventHandler(gl, canvas, bufferId, cBufferId)
  );

  // setup listener untuk melihat jika user menekan clear screen
  document
    .getElementById("clear")
    .addEventListener("click", function clearScreenHandler() {
      index = 0;
      accumulatedPoints = 0;
      shapes = [];
    });

  // setup listener untuk melihat perubahan toggle animate dari user
  document
    .getElementById("animate")
    .addEventListener("click", function animateToggle() {
      animate = !animate;
    });

  render();
}

function render() {
  gl.clear(gl.COLOR_BUFFER_BIT);

  // Animation Toggle, uniform ke sudut sesuai rotasi
  if (animate) {
    theta += 0.1;
    gl.uniform1f(thetaLoc, theta);
  } else {
    theta = 0.0;
    gl.uniform1f(thetaLoc, theta);
  }

  shapes.forEach((object) => {
    if (object.isFilled) {
      gl.drawArrays(
        gl.TRIANGLE_FAN,
        object.startingIndex,
        object.numberOfPoints
      );
    }
    gl.drawArrays(gl.LINE_LOOP, object.startingIndex, object.numberOfPoints);
  });

  // permintaan render, jika animasi maka disesuaikan sesuai timeout
  if (animate) {
    setTimeout(function () {
      requestAnimationFrame(render);
    }, speed);
  } else {
    requestAnimationFrame(render);
  }
}
